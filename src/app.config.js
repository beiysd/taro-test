export default {
  pages: ["pages/Login/index","pages/Test/index", "pages/Bar/index"],
  subpackages: [
    {
      root: "pagesTwo",
      pages: [
        "pages/Echarts/index",
        "pages/Resolution/index",
        
      ],
    },
  ],

  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },

  tabBar: {
    list: [
      {
        pagePath: "pages/Bar/index",
        text: "Bar",
        selectedIconPath: "assets/img/selecthomes.png",
        iconPath: "assets/img/homes.png",
      },
      {
        pagePath: "pages/Test/index",
        text: "Test",
        // selectedIconPath: "",
        // iconPath: ""
      },
    ],
  },
};
