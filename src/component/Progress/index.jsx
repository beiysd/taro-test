/**
 * @name ProgresCanvas
 * @desc 进度条
 * @author wuxd
 * @date 2020/08/06
 */

import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Canvas, Image } from "@tarojs/components";

import  "./index.scss";

let clientHeight = Taro.$navBarWindowHeight; //可使用窗口高
let clientWidth = Taro.$navBarWindowWidth; //可使用窗口宽

let ratio; //根据尺寸动态计算 1px换算成多少rpx

export default class ProgresCanvas extends Component {
  static defaultProps = {
    data: "lalalal",
    color: "#7d79f3",
    progress: 0
  };

  state = { imageUrl: "", nowdata: "" };
componentDidMount(){
  this.drawProgressbg();
}
  componentDidUpdate() {
    this.drawProgressbg();
  }

  //绘制背景
  drawProgressbg = async () => {
    //stepone, steptwo, size, width, colorone, colortwo
    const { data } = this.props;
    const { nowdata } = this.state;
    if (JSON.stringify(data) === JSON.stringify(nowdata)) {
      return;
    } else if (data) {
      try {
        this.setState({ nowdata: data });
        const res = await Taro.getSystemInfo();

        clientWidth = res.windowWidth;
        clientHeight = res.windowHeight;
        console.log('clientHeight==',clientWidth,clientHeight)
        //屏幕宽度 375px = 750rpx, 1px = rpx
        // 1px = （750 / 屏幕宽度）rpx；
        // 1rpx = （屏幕宽度 / 750）px;
        ratio = 550 / clientWidth;

        // 使用 .createContext 获取绘图上下文 context
        const { canvas, ctx } = await this.getCanvasCtx(data.idone);
        // const { canvas, ctx } = {};
        ctx.lineWidth = Number(data.width);
        ctx.strokeStyle = `${data.colorone}`;
        // ctx.lineCap = "butt";
        ctx.beginPath(); //开始一个新的路径
        ctx.arc(69 / ratio, 69 / ratio, 60 / ratio, -Math.PI / data.start, data.end === 1 ? -2 * Math.PI : Math.PI + Math.PI / 2, false); //x,y,半径,开始,结束
        // ctx.closePath();
        ctx.stroke(); //对当前路径进行描边

        ctx.strokeStyle = `${data.colortwo}`;
        ctx.beginPath();
        if (data.end === 1) {
          ctx.arc(69 / ratio, 69 / ratio, 60 / ratio, -Math.PI / data.start, Number(data.steptwo) * Math.PI - Math.PI, false); //x,y,半径,开始,结束
        } else {
          ctx.arc(69 / ratio, 69 / ratio, 60 / ratio, -Math.PI / data.start, Number(data.steptwo) * Math.PI - Math.PI / 2, false); //x,y,半径,开始,结束
        }

        ctx.stroke();

        setTimeout(() => {
          const base64 = canvas.toDataURL("jpg", 0.6);
          this.setState({ imageUrl: base64 });
        }, 500);
      } catch (err) {
        console.error(err);
        const s = err.toString();
        Taro.showModal({ content: s });
      }
    }
  };

  getCanvasCtx = async (eleId = "", delay = 200) => {
    return new Promise((resolve, reject) => {
      const t = setTimeout(() => {
        clearTimeout(t);
        Taro.createSelectorQuery()
          .select(`#${eleId}`)
          .fields({ node: true })
          .exec((res) => {
            if (res && res[0] && res[0].node) {
              const canvas = res[0].node;
              const ctx = canvas.getContext("2d");
              resolve({ canvas, ctx });
            } else {
              reject({});
            }
          });
      }, delay);
    });
  };

  render() {
    const { data } = this.props;
    let lefts = clientWidth > 380 ? "4%" : "9%";
    return (
      <View className="progress_box" style={{ zIndex: 1 }}>
       <Image src={this.state.imageUrl} className="img" style={{ marginLeft: lefts }} /> 

        <Canvas className="progress_bg" type="2d" id={data.idone} canvas-id={data.idone} style={{ marginLeft: lefts }}></Canvas>
      </View>
    );
  }
}
