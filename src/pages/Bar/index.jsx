import Taro from "@tarojs/taro"
import React, { Component } from "react"
import { View, Text, Button } from "@tarojs/components"
import "./index.scss"

export default class PageOne extends Component {
  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  render() {
    return (
      <View className="index">
        <Text>PageOne</Text>
        <Button
          onClick={() => {
            Taro.navigateTo({
              url: "/pagesTwo/pages/Echarts/index"
            })
          }}
        >
          Echarts
        </Button>
        <Button
          onClick={() => {
            Taro.navigateTo({
              url: "/pagesTwo/pages/Resolution/index"
            })
          }}
        >
          Canvas圆环进度条
        </Button>
      </View>
    )
  }
}
