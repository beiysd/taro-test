/**
 * @name Resolution
 * @description
 * @author
 * @date
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text } from "@tarojs/components";

import ProgresCanvas from "@/component/Progress";
import "./index.scss";
export default class Resolution extends Component {
  static defaultProps = {
    data: [],
    type: "",
  };
  state = {
    width: 0, //可使用窗口宽
  };
  async componentDidMount() {
    const res = await Taro.getSystemInfo();
    console.log("res==", res);
    let width = Math.round(0.32 * res.windowWidth);
    this.setState({ width });
  }
  render() {
    const { width } = this.state;
    const progDataOne = {
      stepone: 2,
      steptwo: 1,
      width: 6,
      colorone: "#E7E7E7",
      colortwo: "#8CE1B1",
      idone: "pro0101",
      idtwo: "pro0102",
      start: 2,
    };
    const progDataTwo = {
      stepone: 2,
      steptwo: 1.5,
      width: 6,
      colorone: "#E7E7E7",
      colortwo: "#87C6DF",
      idone: "pro0201",
      idtwo: "pro0202",
      start: 2,
    };
    const progDataThree = {
      stepone: 2,
      steptwo: 0.5,
      width: 6,
      colorone: "#E7E7E7",
      colortwo: "#FFB17D",
      idone: "pro0301",
      idtwo: "pro0302",
      start: 2,
    };
    return (
      <View>
        <View className="title" style={{ paddingTop: "10px" }}>
          解决策略
        </View>
        <View className={"title_next"}>问题出在哪了 ?</View>
        <View className={["at-row"]}>
          <View
            className={["progBody", "at-col at-col-4"]}
            style={{ width: width, height: width }}
          >
            <ProgresCanvas data={progDataOne} width={width} />
          </View>
          <View
            className={["progBody", "at-col at-col-4"]}
            style={{ width: width, height: width }}
          >
            <ProgresCanvas data={progDataTwo} width={width} />
          </View>
          <View
            className={["progBody", "at-col at-col-4"]}
            style={{ width: width, height: width }}
          >
            <ProgresCanvas data={progDataThree} width={width} />
          </View>
        </View>
      </View>
    );
  }
}
